package com.example.sarredon.coordinatorlayoutsandbox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    RecyclerView mRecyclerView;
    View mView;

    int mItemHeight;
    int mMaxHeight;
    float _density;
    Random r;
    View mMap;

    Button mExpand;
    Button mCollapse;
    Button mHide;
    Button mRepopulate;

    BottomSheetBehavior<View> mBottomSheetBehavior;

    CoordinatorLayout mCoordinatorLayout;

    private static final List<String> TEXT = new ArrayList<String>() {{
        addAll(Arrays.asList("text1", "text2", "text3", "text4", "text5", "text6", "text7", "text8", "text9", "text10",
                "text11", "text12", "text13", "text14", "text15", "text16", "text17", "text18", "text19", "text20",
                "text21", "text22", "text23", "text24", "text25", "text26", "text27", "text28", "text29", "text30"));
        }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        final Adapter adapter = new Adapter();
        mRecyclerView.setAdapter(adapter);
/*
        adapter.update(TEXT);
        adapter.notifyDataSetChanged();
*/

        mView = findViewById(R.id.placeholder_bottom_sheet);

        mBottomSheetBehavior = BottomSheetBehavior.from(mView);
        mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetCallback);

        r = new Random();
        mItemHeight = getResources().getDimensionPixelOffset(R.dimen.list_item_height);
        mMaxHeight = getResources().getDimensionPixelOffset(R.dimen.bottom_sheet_height);
        _density = mRecyclerView.getContext().getResources().getDisplayMetrics().density;

        mMap = findViewById(R.id.google_map_container);
        if (mMap != null) {
            mMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "map clicked");
                }
            });
        }

        mExpand = (Button) findViewById(R.id.expand_button);
        mExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        mCollapse = (Button) findViewById(R.id.collapse_button);
        mCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        mHide = (Button) findViewById(R.id.hide_button);
        mHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setHideable(true);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                mBottomSheetBehavior.setHideable(false);
            }
        });

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);

        mRepopulate = (Button) findViewById(R.id.repopulate_button);
        mRepopulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCoordinatorLayout.requestLayout(); // FIXME: 10/7/16 Why doesn't this work when bottom sheet is STATE_HIDDEN?

                //final int newSize = r.nextInt(TEXT.size()) + 1; // [1, TEXT.size]
                //Log.d(TAG, "newSize = " + newSize);

                //final Adapter adapter = (Adapter) mRecyclerView.getAdapter();
                //adapter.update(TEXT.subList(0, newSize));
                //adapter.notifyDataSetChanged();
                //resizeIfNeeded(Math.min(mMaxHeight, mItemHeight * newSize));
                //v.requestLayout();
            }
        });

    }

    private final BottomSheetBehavior.BottomSheetCallback mBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull final View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_EXPANDED:
                    Log.d(TAG, "STATE_EXPANDED");
                    break;
                case BottomSheetBehavior.STATE_COLLAPSED:
                    Log.d(TAG, "STATE_COLLAPSED");
                    break;
                case BottomSheetBehavior.STATE_HIDDEN:
                    Log.d(TAG, "STATE_HIDDEN");
                    break;
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) { }
    };

    private void resizeIfNeeded(int newHeight) {
        final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mRecyclerView.getLayoutParams();
        if (params.height != newHeight) {
            Log.d(TAG, "params.height = " + params.height / _density + "dp, resizing to " + newHeight / _density + "dp");
            params.height = newHeight;
            mRecyclerView.setLayoutParams(params);
        } else {
            Log.d(TAG, "params.height = " + params.height / _density + "dp, no resizing needed");
        }
    }

    public static class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<String> mData = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (!mData.isEmpty()) {
                holder.bind(mData.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public void update(final List<String> data) {
            mData.clear();
            mData.addAll(data);
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.text_view);
        }

        public void bind(String text) {
            mTextView.setText(text);
        }
    }
}
